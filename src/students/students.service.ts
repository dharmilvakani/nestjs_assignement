import { Injectable } from '@nestjs/common';
import { students } from '../dbs'
import { CreateStudentDto, FindStudentsResponseDto, StudentsResponseDto, UpdateStudentDto } from 'src/students/Dto/student.dto';
import { v4 as uuid } from 'uuid'
@Injectable()
export class StudentsService {
    private students = students

    getStudents(): FindStudentsResponseDto[] {
        return this.students
    }
    getStudentById(studentId: string): FindStudentsResponseDto {
        return this.students.find(student => {
            return student.id === studentId
        })
    }
    createStudent(payload: CreateStudentDto): StudentsResponseDto {
        const newStudent = {
            id: uuid(),
            ...payload
        }
        this.students.push(newStudent)
        return newStudent
    }
    updateStudent(payload: UpdateStudentDto, studentId: string) {
        let updatedStudent: StudentsResponseDto;

        const updatedStudensList = this.students.map(student => {
            if (student.id === studentId) {
                updatedStudent = {
                    id: studentId,
                    ...payload
                }
                return updatedStudent
            } else return student
        })
        this.students = updatedStudensList
        return updatedStudent
    }

    getStudentsByTeacherId(teacherId: string): FindStudentsResponseDto[] {
        return this.students.filter(student => {
            return student.teacher
        })
    }

    updateStudentTeacher(teacherId: string, studentId: string): StudentsResponseDto {
        let updatedStudent: StudentsResponseDto;

        const updatedStudensList = this.students.map(student => {
            if (student.id === studentId) {
                updatedStudent = {
                    ...student,
                    teacher: teacherId
                }
                return updatedStudent
            } else return student
        })
        this.students = updatedStudensList
        return updatedStudent
    }

}


