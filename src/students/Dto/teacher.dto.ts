export class CreateTeacher {
    id: string;
    name: string;
}

export class UpdateTeacher {
    id: string;
    name: string;
}

export class FindTeacherResponseDto {
    id: string;
    name: string;
}