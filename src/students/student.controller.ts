import { Body, Controller, Get, Param, Post, Put, ParseUUIDPipe } from "@nestjs/common";
import { StudentsService } from "src/students/students.service";
import { CreateStudentDto, UpdateStudentDto, FindStudentsResponseDto, StudentsResponseDto } from '../students/Dto/student.dto'
@Controller('students')
export class StudentController {

    constructor(private readonly studentService: StudentsService) {

    }

    @Get()
    getStudents(): FindStudentsResponseDto[] {
        return this.studentService.getStudents()
    }

    @Get(":studentId")
    getStudentById(@Param("studentId", new ParseUUIDPipe()) studentId: string
    ): FindStudentsResponseDto {
        return this.studentService.getStudentById(studentId)
    }

    @Post()
    createStudent(@Body() body: CreateStudentDto): StudentsResponseDto {
        return this.studentService.createStudent(body)
    }

    @Put(":studentId")
    updateStudents(
        @Param("studentId", new ParseUUIDPipe()) studentId: string,
        @Body() body: UpdateStudentDto
    ): StudentsResponseDto {
        return this.studentService.updateStudent(body, studentId)
    }
}