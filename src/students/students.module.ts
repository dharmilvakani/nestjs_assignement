import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { validStudentMiddleware } from 'src/common/middleware/validStudent.middleware';
import { StudentController } from 'src/students/student.controller';
import { StudentsService } from 'src/students/students.service';

@Module({
    controllers: [StudentController],
    providers: [StudentsService],
    exports: [StudentsService]
})
export class StudentsModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(validStudentMiddleware).forRoutes({
            path: "students/:studentId",
            method: RequestMethod.GET
        }),
            consumer.apply(validStudentMiddleware).forRoutes({
                path: "students/:studentId",
                method: RequestMethod.PUT
            })
    }
}
