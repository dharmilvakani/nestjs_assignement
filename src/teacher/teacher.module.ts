import { Module } from '@nestjs/common';
import { StudentsModule } from 'src/students/students.module';
import { StudentTeacherController } from 'src/teacher/student.controller';
import { TeacherController } from 'src/teacher/teacher.controller';
import { TeacherService } from 'src/teacher/teacher.service';

@Module({
    imports: [StudentsModule],
    controllers: [TeacherController, StudentTeacherController],
    providers: [TeacherService]
})
export class TeacherModule { }
