import { Body, Controller, Get, Param, Put, ParseUUIDPipe } from '@nestjs/common';
import { FindStudentsResponseDto, StudentsResponseDto } from 'src/students/Dto/student.dto';
import { StudentsService } from 'src/students/students.service';

@Controller("teachers/:teacherId/students")
export class StudentTeacherController {

    constructor(private readonly studentService: StudentsService) { }

    @Get()
    getStudents(@Param("teacherId") teacherId: string): FindStudentsResponseDto[] {
        return this.studentService.getStudents()
    }
    @Put("/:studentId")
    updateStudentTeacher(
        @Param("teacherId", new ParseUUIDPipe()) teacherId: string,
        @Param("studentId", new ParseUUIDPipe()) studentId: string
    ): StudentsResponseDto {
        return this.studentService.updateStudentTeacher(teacherId, studentId)
    }
}
